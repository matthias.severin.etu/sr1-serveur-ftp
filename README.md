# SR1-serveur-ftp / NGUYEN Kévin - SEVERIN Matthias

## Remarques

Toutes les éxigences du TP ont été remplies, sauf le point ACPA, nous avons uniquement géré le mode passif.
A noter, le système sur lequel le serveur ce situe doit être sous Linux et en anglais, pour pouvoir faire la commande ls -al en anglais, sinon lors de l'affichage de la commande LIST, plus rien ne fonctionne car filezilla n'aime pas les accents dans les descriptions de fichiers par exemple.

## Generation de la javadoc

mvn javadoc:javadoc

## Generation du fichier jar

mvn package

## Execution avec sans parametre

java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar

Permet de lancer le serveur sur le port 21, avec comme dossier root, la racine du système, et en autorisant les connexions en anonyme.

## Execution avec 1 parametre

java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar [port]

Permet de lancer le serveur sur le port passé en parametre, avec comme dossier root, la racine du système, et en autorisant les connexions en anonyme.

Exemple : java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar 1234

## Execution avec 2 parametres

java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar [port] [boolean autoriserAnonymes]

Permet de lancer le serveur sur le port passé en parametre, avec comme dossier root, la racine du système, et en autorisant ou non les connexions en anonyme.

Exemple en autorisant les anonymes : java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar 1234 true
Exemple en refusant les anonymes : java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar 1234 false

## Execution avec 3 parametres

java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar [port] [boolean autoriserAnonymes] [root]

Permet de lancer le serveur sur le port passé en parametre, avec comme dossier root, le dossier root passé en parametre, et en autorisant ou non les connexions en anonyme.

Exemple avec /home/toto la racine du serveur : java -jar target/SERVEUR_FTP-1.0-SNAPSHOT.jar 1234 true /home/toto