package fil.sr1.SERVEUR_FTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

	public static void main(String[] args) {
		Properties prop = new Properties();
		File file = new File("user.properties");
		try {
			FileInputStream fileInput = new FileInputStream(file);
			prop.load(fileInput);
		} catch (IOException e) {
			prop = null;
		}

		int port = 21;
		boolean anonymous = true;
		String root = "";

		if (args.length > 0) {
			port = Integer.parseInt(args[0]);

			if (args.length > 1) {
				if (args[1].equals("false")) {
					anonymous = false;
				}

				if (args.length > 2) {
					root = args[2];
				}
			}
		}
		
		new ServerFTP(port, root, prop, anonymous);
		
	}
}
