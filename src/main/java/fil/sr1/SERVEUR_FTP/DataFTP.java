package fil.sr1.SERVEUR_FTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class DataFTP implements Runnable {
	
	protected ServerSocket ss;
	protected Socket socket;
	protected OutputStream out;
	protected PrintWriter printer;
	protected BufferedReader reader;
	protected InputStream in;

	/**
	 * Create a data socket
	 * @param port the port to connect.
	 * @throws IOException if it fail to create the socket.
	 */
	public DataFTP(int port) throws IOException {
		ss = new ServerSocket(port);
	}
	

	/**
	 * start the data socket.
	 * FTPException : if it fail to start the data socket.
	 */
	@Override
	public void run() {
		try {
			socket = ss.accept();
			
			out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			
			in = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(in);
			reader = new BufferedReader(isr);
		} catch (IOException e) {
			throw new FTPException("Unable to start the data socket");
		}
	}
	
	/**
	 * Send the message msg to the client.
	 * @param msg the message to send to the client.
	 */
	public void write(String msg) {
		printer.println(msg);
	}
	
	/**
	 * Read the message sent by the client.
	 * @return the message sent by the client.
	 * FtpException : thrown if it fail to read the messsage sent by the client.
	 */
	public String readLine() {
		try {
			String res = reader.readLine();
			return res;
		} catch (IOException e) {
			throw new FTPException("Cannot read the message");
		}
	}
	
	/**
	 * Close the connection
	 * FtpException : thrown if it fail to close the connection.
	 */
	public void close() {
		try {
			printer.close();
			reader.close();
			socket.close();
			ss.close();
		} catch (IOException e) {
			throw new FTPException("Failed to close the data socket");
		}
	}
	
	/**
	 * Gather the informations of all files of a directory given in parameter.
	 * @param directory the directory from which gather the informations.
	 * @return a list containing the information of each file in the directory.
	 */
	public static ArrayList<String> getAllDescriptionsFiles(String directory) {
		String description;
        Process process;
        try {
            process = Runtime.getRuntime().exec("ls -l " + directory);
            BufferedReader buffReader = new BufferedReader(
            new InputStreamReader(process.getInputStream()));
			ArrayList<String> listDescriptions = new ArrayList<String>();
            while ((description = buffReader.readLine()) != null) {
                listDescriptions.add(description);
			}
            listDescriptions.remove(0);
            process.waitFor();
            process.destroy();
			return listDescriptions;
        } catch (Exception e) {
			throw new FTPException("Unable to get the directory description");
		}
	}

}
