package fil.sr1.SERVEUR_FTP;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.io.FileOutputStream;

public class ClientFTP implements Runnable {
	
	protected Socket socket;
	protected PrintWriter printer;
	protected BufferedReader reader;
	private boolean allowAnonymous;
	private Properties user;
	private boolean debug;
	private String username;
	private boolean connected = false;
	private DataFTP data;
	private String root;
	private String directory = "/";
	private TransfertType type = TransfertType.ASCII;
	private String oldName = "";
	
	/**	
	 * Create a connection to a client
	 * @param socket the socket to which to get the command and send data
	 * @param root the root folder that serve as the data folder
	 * @param user the list of users allowed to connect to the server.
	 * @param allowAnonymous if the server must allow the client to connect as anonymous
	 * @param debug print the informations, commands in the console
	 * FtpException : thrown if it fail to start the client's thread
	 */
	public ClientFTP(Socket socket, String root, Properties user, boolean allowAnonymous, boolean debug) {
		this.debug = debug;
		this.root = root;
		this.socket = socket;
		this.allowAnonymous = allowAnonymous;
		this.user = user;
		
		try {
			OutputStream out = socket.getOutputStream();
			printer = new PrintWriter(out, true);
			
			InputStream in = socket.getInputStream();
			InputStreamReader isr= new InputStreamReader(in);
			reader = new BufferedReader(isr);
		} catch (IOException e) {
			throw new FTPException("Failed to start the client's thread");
		}
	}

	/**
	 * Close the connection.
	 * FtpException : thrown if it fail to close the connection
	 */
	public void close() {
		try {
			this.printer.close();
			this.reader.close();
			this.socket.close();
		} catch (IOException e) {
			throw new FTPException("Failed to close the client's socket");
		}
	}

	/**
	 * Send the message msg to the client.
	 * @param msg the message to send to the client.
	 */
	public void write(String msg) {
		printer.println(msg);
	}
	
	/**
	 * Read the message sent by the client.
	 * @return the message sent by the client.
	 * FtpException : thrown if it fail to read the messsage sent by the client.
	 */
	public String readLine() {
		try {
			String res = reader.readLine();
			if (debug) {
				System.out.println(res);
			}
			return res;
		} catch (IOException e) {
			throw new FTPException("Cannot read the message");
		}
	}

	/**
	 * Check for commands sent by the client.
	 */
	@Override
	public void run() {
		connect();
		while (true) {
			String msg = readLine();
			if (msg == null) {
				this.close();
				Thread.currentThread().interrupt();
				return;
			}
			if (msg.startsWith("USER")) {
				user(msg);
			} else if (msg.startsWith("PASS")) {
				pass(msg);
			} else if (connected == false) {
				write("530 Please login with USER and PASS.");
			} else {
				if (msg.startsWith("AUTH")) {
					auth(msg);
				} else if (msg.startsWith("SYST")) {
					syst(msg);
				} else if (msg.startsWith("FEAT")) {
					feat(msg);
				} else if (msg.startsWith("PWD")) {
					pwd(msg);
				} else if (msg.startsWith("TYPE")) {
					type(msg);
				} else if (msg.startsWith("PASV")) {
					pasv(msg);
				}  else if (msg.startsWith("LIST")) {
					list(msg);
				} else if (msg.startsWith("CWD")) {
					cwd(msg);
				} else if (msg.startsWith("CDUP")) {
					cdup(msg);
				} else if (msg.startsWith("RNFR")) {
					rnfr(msg);
				} else if (msg.startsWith("RNTO")) {
					rnto(msg);
				} else if (msg.startsWith("MKD")) {
					mkd(msg);
				} else if (msg.startsWith("DELE")) {
					dele(msg);
				} else if (msg.startsWith("STOR")) {
					stor(msg);
				} else if (msg.startsWith("RMD")) {
					rmd(msg);
				} else if (msg.startsWith("RETR")) {
					retr(msg);
				}
			}
		}
	}
	
	
	/**
	 * STOR command allow to upload data to the server, stor method
	 * launch the right transfert method depending of the transfert type.
	 * @param msg the STOR command containing the name of the file to upload sent by the client.
	 */
	private void stor(String msg) {
		if (type == TransfertType.ASCII) {
			storAscii(msg);
		} else if (type == TransfertType.BINARY) {
			storBinary(msg);
		} else {
			write("504 Type not implemented.");
		}
	}

	/**
	 * Upload data to the server using binary transfert.
	 * @param msg the STOR command containing the name of the file to upload sent by the client.
	 */
	private void storBinary(String msg) {
		String[] msgSplit = msg.split(" ");
		write("150 Ok to send data.");
		try {
			File file = new File(root + directory + "/" + msgSplit[1]);
			FileOutputStream fileOutput = new FileOutputStream(file);
			byte[] buffer = new byte[4096];
			int line = data.in.read(buffer);
			while (line != -1) {
				fileOutput.write(buffer);
				line = data.in.read(buffer);
			}
			fileOutput.close();
			data.close();
			write("226 Transfer complete.");
		} catch (IOException e) {
			write("553 Failed to transfert the file");
		}
	}

	/**
	 * Upload data to the server using ASCII transfert.
	 * @param msg the STOR command containing the name of the file to upload sent by the client.
	 */
	private void storAscii(String msg) {
		String[] msgSplit = msg.split(" ");
		write("150 Ok to send data.");
		try {
			FileWriter file = new FileWriter(root + directory + "/" + msgSplit[1]);
			String line = data.readLine();
			while (line != null) {
				file.write(line + "\n");
				line = data.readLine();
			}
			file.close();
			data.close();
			write("226 Transfer complete.");
		} catch (IOException e) {
			write("553 Failed to transfert the file");
		}
	}

	/**
	 * Download data to the client, retr method,
	 * launch the right transfert method depending of the transfert type.
	 * @param msg the RETR command containing the name of the file to download sent by the client.
	 */
	private void retr(String msg) {
		if (type == TransfertType.ASCII) {
			retrAscii(msg);
		} else if (type == TransfertType.BINARY) {
			retrBinary(msg);
		} else {
			write("504 Type not implemented.");
		}
	}
	
	/**
	 * Download data to the client using Binary transfert.
	 * @param msg the RETR command containing the name of the file to download sent by the client.
	 */
	private void retrBinary(String msg) {
		String[] msgSplit = msg.split(" ");
		write("150 Ok to send data.");
		try {
			File file = new File(root + directory + "/" + msgSplit[1]);
			FileInputStream fileInput = new FileInputStream(file);
			byte[] buffer = new byte[4096];
			int line = fileInput.read(buffer);
			while (line != -1) {
				data.out.write(buffer);
				line = fileInput.read(buffer);
			}
			fileInput.close();
			data.close();
			write("226 Transfer complete.");
		} catch (IOException e) {
			write("553 Failed to transfert the file");
		}
	}

	/**
	 * Download data to the client using ASCII transfert.
	 * @param msg the RETR command containing the name of the file to download sent by the client.
	 */
	private void retrAscii(String msg) {
		String[] msgSplit = msg.split(" ");
		write("150 Ok to send data.");
		try {
			File file = new File(root + directory + "/" + msgSplit[1]);
			Scanner reader = new Scanner(file);
			while (reader.hasNextLine()) {
				String line = reader.nextLine();
				data.write(line);
			}
			reader.close();
			data.close();
			write("226 Transfer complete.");
		} catch (FileNotFoundException e) {
			write("553 Failed to transfert the file");
		}
	}

	/**
	 * Command to go to the parent folder.
	 * @param msg the CDUP command sent by the client.
	 */
	private void cdup(String msg) {
		int lastIndex = directory.lastIndexOf("/");
		if (lastIndex == 0) {
			write("550 You cannot go beyond the root directory.");
		} else {
			directory = directory.substring(0, lastIndex);
			write("250 Directory successfully changed.");
		}
	}

	/**
	 * Move to an another folder.
	 * @param msg the CWD command sent by the client containing the name of the folder to move.
	 */
	private void cwd(String msg) {
		String[] msgSplit = msg.split(" ");
		String newDir;
		if (msgSplit[1].startsWith("/")) {
			newDir = msgSplit[1];
		} else {
			newDir = directory + "/" + msgSplit[1];
		}
		File dir = new File(root + newDir);
		if (dir.isDirectory()) {
			directory = newDir;
			write("250 Directory successfully changed.");
		} else {
			write("550 Wrong directory.");
		}
	}

	/**
	 * List all the files in the folder.
	 * @param msg the LIST command sent by the client.
	 */
	private void list(String msg) {
		ArrayList<String> descriptions = DataFTP.getAllDescriptionsFiles(root + directory);
		write("150 Here comes the directory listing.");
		try {
			for (String description : descriptions) {
				data.write(description);
			}
			data.close();
			write("226 Directory send OK.");
		} catch (FTPException e) {
			write("550 Failed to send the directory.");
		}
	}

	/**
	 * Inform the client of being connected to the server.
	 */
	private void connect() {
		write("220 FTP server");
	}

	/**
	 * Create a data connection, and inform the client of the ip and port to connect.
	 * @param msg the PASV command sent by the client.
	 */
	private void pasv(String msg) {
		Random random = new Random();
		int port1 = random.nextInt(240) + 10;
		int port2 = random.nextInt(256);
		try {
			data = new DataFTP(port1 * 256 + port2);
			Thread thread = new Thread(data);
			thread.start();
			write("227 Entering Passive Mode (127,0,0,1," + port1 + "," + port2 + ").");
			thread.join();
		} catch (Exception e) {
			write("500 Failed to open the new socket");
		}
	}

	/**
	 * Switch type of transfert.
	 * @param msg the TYPE command sent by the client.
	 */
	private void type(String msg) {
		String[] msgSplit = msg.split(" ");
		if (msgSplit[1].equals("I")) {
			type = TransfertType.BINARY;
			write("200 Switching to Binary mode.");
		} else if (msgSplit[1].equals("A")) {
			type = TransfertType.ASCII;
			write("200 Switching to ASCII mode.");
		} else {
			write("504 Type not implemented.");
		}
	}

	/**
	 * Inform of the client of the path of the current folder.
	 * @param msg the PWD command sent by the client.
	 */
	private void pwd(String msg) {
		write("257 \"" + directory + "\" is the current directory.");
	}

	/**
	 * Inform the client of the featured command by the server.
	 * @param msg the FEAT sent by the client.
	 */
	private void feat(String msg) {
		write("211-Features:");
		write("PASV");
		write("211 End");
	}

	/**
	 * Inform the client of the system on which the server is installed.
	 * @param msg the SYST command sent by the client.
	 */
	private void syst(String msg) {
		write("215 UNIX");
	}

	/**
	 * Check if the user can connect to the server.
	 * @param msg the PASS command sent by the user containing the user password.
	 */
	private void pass(String msg) {
		String[] msgSplit = msg.split(" ");
		String password = msgSplit[1];
		if (username.equals("anonymous")) {
			if (allowAnonymous) {
				connected = true;
				write("230 Login successful.");
			} else {
				write("530 This FTP server doesn't allow anonymous.");
			}
		} else {
			if (user == null) {
				write("530 Wrong password");
			} else {
				String userPassword = user.getProperty(username);
				if (password.equals(userPassword)) {
					connected = true;
					write("230 Login successful.");
				} else {
					write("530 Wrong password");
				}
			}
		}
	}

	/**
	 * Gather the name of the user.
	 * @param msg the USER command sent by the client containing the username.
	 */
	private void user(String msg) {
		String[] msgSplit = msg.split(" ");
		username = msgSplit[1];
		write("331 Please specify the password.");
	}

	/**
	 * Command not implemented.
	 * @param msg the AUTH command sent by the client.
	 */
	private void auth(String msg) {
		write("502 Command not implemented.");
	}

	/**
	 * Check if the file can be renamed
	 * @param msg the RNFR command sent by the client containing the name of the file to rename.
	 */
	private void rnfr(String msg) {
		String[] msgSplit = msg.split(" ");
		File fileOldName = new File(root + directory + "/" + msgSplit[1]);
		if(fileOldName.exists()) {
			this.oldName = msgSplit[1];
			write("350 Ready for RNTO.");
		}
		else {
			write("550 File not found.");
		}
	}

	/**
	 * Rename a file.
	 * @param msg the RNTO command sent by the client containing the name to which rename the file.
	 */
	private void rnto(String msg) {
		if (!this.oldName.equals("")) {
			String[] msgSplit = msg.split(" ");
			File fileNewName = new File(root + directory + "/" + msgSplit[1]);
			File fileOldName = new File(root + directory + "/" + this.oldName);
			boolean success = fileOldName.renameTo(fileNewName);
			if (success == true) {
				this.oldName = "";
				write("250 Rename successful.");
			} else {
				write("550 Permission denied.");
			}
		} else {
			write("550 File not found.");
		}
	}

	/**
	 * Create a new folder.
	 * @param msg the command MKD sent by the client containing the name of the folder to create.
	 */
	private void mkd(String msg) {
		String[] msgSplit = msg.split(" ");
		File newDirectory = new File(root + directory + "/" + msgSplit[1]);
		if (!newDirectory.exists()) {
			newDirectory.mkdir();
			write("257 " + directory + "/" + msgSplit[1] + " created.");
		}
		else {
			write("553 This directory already exist.");
		}
	}

	/**
	 * Remove a folder.
	 * @param msg the command RMD sent by the client containing the name of the folder to remove.
	 */
	private void rmd(String msg) {
		String[] msgSplit = msg.split(" ");
		File directoryToRemove = new File(root + directory + "/" + msgSplit[1]);
		if (directoryToRemove.isDirectory()) {
			directoryToRemove.delete();
			write("250 directory successfully removed.");
		}
		else {
			write("553 Not a directory.");
		}
	}

	/**
	 * Remove a file.
	 * @param msg the command DELE sent by the client containing the name of the file to remove.
	 */
	private void dele(String msg) {
		String[] msgSplit = msg.split(" ");
		File fileToRemove = new File(root + directory + "/" + msgSplit[1]);
		if (!fileToRemove.isDirectory()) {
			fileToRemove.delete();
			write("250 file successfully removed.");
		}
		else {
			write("553 Not a file.");
		}
	}
}
