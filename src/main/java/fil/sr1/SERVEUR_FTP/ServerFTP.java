package fil.sr1.SERVEUR_FTP;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Properties;

public class ServerFTP {

	protected ServerSocket ss;
	protected ArrayList<Thread> clients = new ArrayList<>();
	

	/**
	 * Create a server socket.
	 * @param port the port to which the server socket will connect
	 * @param root the root folder that serve as the data folder
	 * @param prop the list of users allowed to connect to the server.
	 * @param anonymous true if the server allow anonymous, else false.
	 * FtpException : thrown if it fail to start the FTP server
	 */
	public ServerFTP(int port, String root, Properties prop, boolean anonymous) {
		try {
			ss = new ServerSocket(port);
			
			while (true) {
				Socket socket = ss.accept();
				boolean debug = true;
				ClientFTP run = new ClientFTP(socket, root, prop, anonymous, debug);
				Thread thread = new Thread(run);
				clients.add(thread);
				thread.start();
			}
		} catch (IOException e) {
			throw new FTPException("Failed to start the FTP server");
		}
	}
}
